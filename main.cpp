#include <iostream>
#include <fstream>

using namespace std;

int main() {
    fstream file;

    file.open("C:\\Programs\\text.txt", ios::in);
    if (file.good()) {
        while (!file.eof()) {
            string line;
            int uppercase_chars = 0;
            getline(file, line);
            for (char i : line) {
                if (isupper(i)) {
                    uppercase_chars++;
                }
            }

            if (uppercase_chars >= 2) {
                cout << "Imie i nazwisko: ";
            }

            if (uppercase_chars == 1) {
                cout << "Przedmiot: ";
            }

            if (uppercase_chars == 0 && line.length() <= 2) {
                cout << "Ocena: ";
            }

            if (uppercase_chars == 0 && line.length() > 2 && line[0] != '-') {
                cout << "Data: ";
            }

            cout << line << endl;
        }
    } else {
        cout << "Plik nie istnieje" << endl;
    }
    file.close();

    return 0;
}
